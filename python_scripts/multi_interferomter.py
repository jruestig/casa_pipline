default("simobserve")
project="multi_interferometer"

# Model sky = Halpha image of M51
skymodel         =  "./messier83.fits"

incell="2.8500arcsec"
incell="0.28500arcsec"
indirection='J2000 13h37m00.78s -29d51m58.6s'
inbright="2.07"
incenter="330GHz"
inwidth="50MHz"
complist  =''


# Multi Interferometer pointings

# 12 m array
setpointings       =  True
integration        =  "10s"
direction          =  indirection
mapsize            =  "40arcsec"
maptype            =  "hex"
pointingspacing    =  "1.0PB"
antennalist        =  'alma.out10.cfg'

obsmode            =  "int"
refdat             =  '2000/01/01'
hourangle          =  'transit'

totaltime          =  '0.5h'
calflux            =  '1Jy'

thermalnoise       = 'tsys-atm'
user_pwv           =  0.5
t_ground           = 269.0
seed               = 11111

leakage            = 0.0
#graphics           = 'both'
verbose            = False
overwrite          = True
simobserve()
