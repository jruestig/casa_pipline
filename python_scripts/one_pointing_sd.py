default("simobserve")
project="one_pointing_sd"

# Model sky = Halpha image of M51
skymodel         =  "../messier83.fits"

incell="2.8500arcsec"
incell="0.28500arcsec"
indirection='J2000 13h37m00.78s -29d51m58.6s'
inbright="1."
incenter="990GHz"
inwidth="50MHz"
# refdate="2000/01/01"
complist  =''

# new single dish
setpointings       = True
integration        = "10s"
direction          = indirection
integration        =  "10s"
mapsize            =  "1.1arcmin"
maptype            =  "hex"
obsmode            = "sd"
sdantlist          = "aca.tp.cfg"
sdant              = 0
refdate            = "2012/12/01"
totaltime          =  "8h"
simobserve()


# Multi Interferometer pointings
incenter="110GHz"
inwidth="50MHz"

# 12 m array
setpointings       =  True
integration        =  "10s"
direction          =  indirection
mapsize            =  "40arcsec"
maptype            =  "hex"
pointingspacing    =  "1.0PB"
antennalist        =  'alma.out10.cfg'

obsmode            =  "int"
refdat             =  '2000/01/01'
hourangle          =  'transit'

totaltime          =  '0.5h'
calflux            =  '1Jy'

thermalnoise       = 'tsys-atm'
user_pwv           =  0.5
t_ground           = 269.0
seed               = 11111

leakage            = 0.0
#graphics           = 'both'
verbose            = False
overwrite          = True
simobserve()
