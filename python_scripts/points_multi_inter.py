default("simobserve")
project="points_mi"

# Model sky = Halpha image of M51
skymodel         =  "./sky_on_points.fits"

incell="2.8500arcsec"
incell="0.28500arcsec"
indirection='J2000 13h37m00.78s -29d51m58.6s'
inbright="1."
complist  =''


# Multi Interferometer pointings
incenter="100GHz"
inwidth="50MHz"

# 12 m array
setpointings       =  True
integration        =  "10s"
direction          =  indirection
mapsize            =  "120arcsec"
maptype            =  "hex"
pointingspacing    =  "1.0PB"
antennalist        =  'alma.out10.cfg'

obsmode            =  "int"
refdat             =  '2000/01/01'
hourangle          =  'transit'

totaltime          =  '0.5h'
calflux            =  '1Jy'

thermalnoise       = 'tsys-atm'
user_pwv           =  0.5
t_ground           = 269.0
seed               = 11111

leakage            = 0.0
#graphics           = 'both'
verbose            = False
overwrite          = True
simobserve()
