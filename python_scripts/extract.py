from globalnewton import DataHandler
from os import makedirs
from os.path import exists, join


indir = "./multi_interferometer"
outdir = join(indir, "data_out")

if not exists(outdir):
    makedirs(outdir)

dh = DataHandler(join(indir, "multi_interferometer.alma.out10.ms"))

print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()

for i in range(length):
    dh.save_field(i, outdir)


indir = "./one_pointing_sd/"
outdir = join(indir, "data_out")

if not exists(outdir):
    makedirs(outdir)

dh = DataHandler(join(indir, "one_pointing_sd.alma.out10.ms"))

print("extract pointings....")
dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()

dh.save_field(0, outdir)


indir = "./points_sd/"
outdir = join(indir, "data_out")
if not exists(outdir):
    makedirs(outdir)

dh = DataHandler(join(indir, "points_sd.alma.out10.ms"))
print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()

for i in range(length):
    dh.save_field(i, outdir)

# Noisy Points
indir = "./points_sd/"
outdir = join(indir, "noisy_data_out")
if not exists(outdir):
    makedirs(outdir)
dh = DataHandler(join(indir, "points_sd.alma.out10.noisy.ms"))
print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()
for i in range(length):
    dh.save_field(i, outdir)


# Multi inter sd on m83
ms = "multi_sd"
indir = "./{}/".format(ms)
outdir = join(indir, "data_out")
if not exists(outdir):
    makedirs(outdir)
dh = DataHandler(join(indir, "{}.alma.out10.ms".format(ms)))
print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()

for i in range(length):
    dh.save_field(i, outdir)

# Noisy Points
indir = "./{}/".format(ms)
outdir = join(indir, "noisy_data_out")
if not exists(outdir):
    makedirs(outdir)
dh = DataHandler(join(indir, "{}.alma.out10.noisy.ms".format(ms)))
print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()
for i in range(length):
    dh.save_field(i, outdir)

indir = "./points_mi/"
outdir = join(indir, "data_out")
if not exists(outdir):
    makedirs(outdir)

dh = DataHandler(join(indir, "points_mi.alma.out10.ms"))
print("extract pointings....")
length = dh.extract_pointings(outdir)
print("extract spectral window....")
dh.extract_spectral_window(outdir)
print("read data....")
dh.read_data("DATA")
print("select spectral window....")
dh.select_spectral_window(0)
print("apply flagging....")
dh.apply_flagging()

for i in range(length):
    dh.save_field(i, outdir)
