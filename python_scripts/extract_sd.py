from os import makedirs
from os.path import basename, exists, isdir, join

import numpy as np

from pyrap.tables import table, taql


def extract_data_sd(ms, pol_fst, pol_snd, sw_number, outdir):
    ms_name = basename(ms)[:-3]
    if not isdir(ms):
        raise RuntimeError('Input not found: {}'.format(ms))

    if not exists(outdir):
        makedirs(outdir)

    # Import Single dish measurement, FLOAT_DATA in ALMA.ms, DATA in Casa.ms
    t = table(ms, readonly=True)
    t1 = taql("select DATA from $t where DATA_DESC_ID=$sw_number")
    sd_data = t1.getcol("DATA")
    # t1 = taql("select FLOAT_DATA from $t where DATA_DESC_ID=$sw_number")
    # sd_data = t1.getcol("FLOAT_DATA")
    t2 = taql("select SIGMA from $t where DATA_DESC_ID=$sw_number")
    sd_sigma = t2.getcol("SIGMA")
    sw = t.getcol("DATA_DESC_ID")
    t.close()

    tsw = table(join(ms, "SPECTRAL_WINDOW"))
    band = tsw.getcol("CHAN_FREQ")
    tsw.close()

    sd_var = sd_sigma[:, pol_fst]**2 + sd_sigma[:, pol_snd]**2
    sw_mask = sw == sw_number

    tpointing = table(ms + "/POINTING", readonly=True)
    sd_pointing = tpointing.getcol("DIRECTION")
    tpointing.close()

    sd_pointing = sd_pointing[sw_mask]

    print("Shape: sd_data {}, sd_var {}, sd_pointing {}.".
          format(sd_data.shape, sd_var.shape, sd_pointing.shape))

    npys = [sd_data, sd_var, sd_pointing, band]
    names = ['data', 'var', 'pointing', "band"]
    np.savez_compressed(
        join(outdir, ms_name + '.npz'), **dict(zip(names, npys)))


ms = "one_pointing_sd"
ms_path = join("./{}/".format(ms), "points_sd.aca.tp.sd.ms")
outdir = join("./{}/".format(ms), "data_out")
assert exists(outdir)
outdir = join(outdir, "sd_data")
makedirs(outdir)
extract_data_sd(ms_path, 0, -1, 0, outdir)

ms = "points_sd"
ms_path = join("./{}/".format(ms), "points_sd.aca.tp.sd.ms")
outdir = join("./{}/".format(ms), "data_out")
assert exists(outdir)
outdir = join(outdir, "sd_data")
makedirs(outdir)
extract_data_sd(ms_path, 0, -1, 0, outdir)

ms = "points_sd"
ms_path = join("./{}/".format(ms), "points_sd.aca.tp.noisy.sd.ms")
outdir = join("./{}/".format(ms), "noisy_data_out")
assert exists(outdir)
outdir = join(outdir, "sd_data")
makedirs(outdir)
extract_data_sd(ms_path, 0, -1, 0, outdir)


ms = "multi_sd"
ms_path = join("./{}/".format(ms), "{}.aca.tp.noisy.sd.ms".format(ms))
outdir = join("./{}/".format(ms), "noisy_data_out")
assert exists(outdir)
outdir = join(outdir, "sd_data")
makedirs(outdir)
extract_data_sd(ms_path, 0, -1, 0, outdir)

ms = "multi_sd"
ms_path = join("./{}/".format(ms), "{}.aca.tp.sd.ms".format(ms))
outdir = join("./{}/".format(ms), "data_out")
assert exists(outdir)
outdir = join(outdir, "sd_data")
makedirs(outdir)
extract_data_sd(ms_path, 0, -1, 0, outdir)
