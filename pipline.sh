#!/bin/sh

CASA=/home/j/pro/astrotools/casa-release-5.1.2-4.el7/bin/casa

$CASA --log2term --nogui --nologfile --nologger -c python_scripts/one_pointing_sd.py
$CASA --log2term --nogui --nologfile --nologger -c python_scripts/multi_sd.py
$CASA --log2term --nogui --nologfile --nologger -c python_scripts/multi_interferomter.py
$CASA --log2term --nogui --nologfile --nologger -c python_scripts/points_sd.py
$CASA --log2term --nogui --nologfile --nologger -c python_scripts/points_multi_inter.py

rm *log
rm *last

source /home/j/tutorial-env/bin/activate
python3 python_scripts/extract.py
deactivate

python2 python_scripts/extract_sd.py

cp -r one_pointing_sd/data_out m83_inter_sd/
cp -r one_pointing_sd/*png m83_inter_sd/
rm -r one_pointing_sd

cp -r multi_interferometer/data_out m83_multi_inter
cp -r multi_interferometer/*png m83_multi_inter
rm -r multi_interferometer

cp -r points_sd/*png points_inter_sd
cp -r points_sd/data_out points_inter_sd
cp -r points_sd/noisy_data_out points_inter_sd
rm -r points_sd

mkdir m83_multi_sd
cp -r multi_sd/*png m83_multi_sd
cp -r multi_sd/data_out m83_multi_sd
cp -r multi_sd/noisy_data_out m83_multi_sd
rm -r multi_sd

# cp -r points_mi/*png points_multi_inter
# cp -r points_mi/data_out points_multi_inter
# rm -r points_mi
